package com.springmanger.dtos;

import com.springmanger.models.enums.RoomType;
import com.springmanger.models.groupedmodels.GuestGroup;
import java.sql.Timestamp;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entity that will hold information about the room's state.
 */
@NoArgsConstructor
@Getter
@Setter
public class MakeReservationDto {

  private int roomNumber;
  private RoomType roomType;
  private Timestamp reservedFrom;
  private Timestamp reservedTo;
  private GuestGroup guestGroup;
  private int reservationPrice;
}
