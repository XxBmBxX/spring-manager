package com.springmanger.dtos;

import com.springmanger.models.UserCredentials;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The data transfer object we return to the front end once the
 * user is authenticated.
 */
@NoArgsConstructor
@Getter
@Setter
public class UserDto {

  private String username;
  private String firstName;
  private String lastName;

  /**
   * Constructor with args.
   *
   * @param userCredentials the user credentials
   */
  public UserDto(UserCredentials userCredentials) {

    this.firstName = userCredentials.getFirstName();
    this.lastName = userCredentials.getLastName();
    this.username = userCredentials.getUsername();
  }
}
