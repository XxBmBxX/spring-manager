package com.springmanger.dtos;

import lombok.Getter;
import lombok.Setter;

/**
 * The data transfer object we get from the front end once the
 * user is authenticated.
 */
@Getter
@Setter
public class AuthenticationDto {

  private String username;
  private String token;
}
