package com.springmanger.dtos;

import lombok.Getter;
import lombok.Setter;

/**
 * The data transfer object we return to the front end once the
 * user is authenticated.
 */
@Getter
@Setter
public class LoginCredentialsDto {

  private String username;
  private String password;
}