package com.springmanger.repositories;

import com.springmanger.models.Room;
import com.springmanger.models.groupedmodels.GuestGroup;
import java.util.Set;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * The repository for rooms.
 */
@Repository
public interface RoomRepository extends CrudRepository<Room, Long> {
  Room getRoomByRoomNumber(int roomNumber);

  Iterable<Room> getRoomsByGuestGroupIn(Set<GuestGroup> guestGroupSet);
}
