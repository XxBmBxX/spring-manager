package com.springmanger.repositories;

import com.springmanger.models.Guest;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * The repository for guest.
 */
@Repository
public interface GuestRepository extends CrudRepository<Guest, Long> {
  Optional<Guest> findGuestByFirstNameAndLastName(String firstName, String lastName);
}
