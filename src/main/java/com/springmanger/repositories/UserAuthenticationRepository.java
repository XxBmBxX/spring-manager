package com.springmanger.repositories;

import com.springmanger.models.UserAuthentication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for the user authentication entity.
 */
@Repository
public interface UserAuthenticationRepository extends CrudRepository<UserAuthentication, Long> {
  Iterable<UserAuthentication> findAllByUsername(String username);
}
