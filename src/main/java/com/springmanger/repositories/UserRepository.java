package com.springmanger.repositories;

import com.springmanger.models.UserCredentials;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * User repository.
 */
@Repository
public interface UserRepository extends JpaRepository<UserCredentials, Long> {
  @Query("SELECT CASE WHEN COUNT(u) > 0 THEN true ELSE false END FROM user_credentials u WHERE u.username = :username")
  boolean existsByUsername(@Param("username") String username);

  Optional<UserCredentials> findUserCredentialsByUsername(String username);
}
