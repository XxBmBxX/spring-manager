package com.springmanger.repositories;

import com.springmanger.models.Guest;
import com.springmanger.models.groupedmodels.GuestGroup;
import java.util.Set;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * The repository for the guest group.
 */
@Repository
public interface GuestGroupRepository extends CrudRepository<GuestGroup, Long> {
  Iterable<GuestGroup> findAllByGuestsIn(Set<Guest> guests);
}
