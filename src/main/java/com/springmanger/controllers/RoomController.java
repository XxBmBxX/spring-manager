package com.springmanger.controllers;

import com.springmanger.dtos.MakeReservationDto;
import com.springmanger.exceptions.GuestDoesNotExistException;
import com.springmanger.models.Room;
import com.springmanger.services.AuthenticationService;
import com.springmanger.services.RoomService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

/**
 * The controller that has mappings for the registration requests.
 */
@RequiredArgsConstructor
@RestController
public class RoomController {

  private static final String USER_TOKEN = "user-token";
  private final RoomService roomService;

  private final AuthenticationService authenticationService;

  /**
   * Method that adds a room.
   *
   * @param userToken authentication token
   * @param room room to create
   * @return created room
   */
  @PostMapping("/room")
  public ResponseEntity<Room> addRoom(@RequestHeader(USER_TOKEN) String userToken, @RequestBody Room room) {
    if (authenticationService.isAuthenticated(userToken)) {
      return new ResponseEntity<>(roomService.addRoom(room), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  /**
   * Method that creates a reservation.
   *
   * @param userToken authentication token
   * @param makeReservationDto reservation dto
   * @return room if everything is ok
   */
  @PostMapping("/make-reservation")
  public ResponseEntity<Room> makeReservation(@RequestHeader(USER_TOKEN) String userToken, @RequestBody MakeReservationDto makeReservationDto)
          throws GuestDoesNotExistException {
    if (authenticationService.isAuthenticated(userToken)) {
      return new ResponseEntity<>(roomService.makeReservation(makeReservationDto), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  /**
   * Method that deletes all rooms.
   *
   * @param userToken authentication token
   * @param roomIds room ids
   * @return true or false
   */
  @DeleteMapping("/delete-rooms")
  public ResponseEntity<Boolean> deleteRooms(@RequestHeader(USER_TOKEN) String userToken, @RequestBody Iterable<Long> roomIds) {
    if (authenticationService.isAuthenticated(userToken)) {
      roomService.deleteRooms(roomIds);
      return new ResponseEntity<>(true, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  /**
   * Method that returns all rooms.
   *
   * @param userToken user token in the headers
   * @return rooms
   */
  @GetMapping("/rooms")
  public ResponseEntity<Iterable<Room>> getAllRooms(@RequestHeader(USER_TOKEN) String userToken) {
    if (authenticationService.isAuthenticated(userToken)) {
      return new ResponseEntity<>(roomService.getAllRooms(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  /**
   * Method that marks rooms as clean.
   *
   * @param userToken authentication token
   * @param roomIds rooms to be marked as clean
   * @return the marked rooms
   */
  @PatchMapping("/mark-rooms-as-clean")
  public ResponseEntity<Iterable<Room>> markRoomsAsClean(@RequestHeader(USER_TOKEN) String userToken, @RequestBody Iterable<Long> roomIds) {
    if (authenticationService.isAuthenticated(userToken)) {
      return new ResponseEntity<>(roomService.markRoomsAsClean(roomIds), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  /**
   * Method that marks rooms as empty.
   *
   * @param userToken authentication token
   * @param roomIds rooms to be marked as clean
   * @return the marked rooms
   */
  @PatchMapping("/mark-rooms-as-empty")
  public ResponseEntity<Iterable<Room>> markRoomsAsEmpty(@RequestHeader(USER_TOKEN) String userToken, @RequestBody Iterable<Long> roomIds) {
    if (authenticationService.isAuthenticated(userToken)) {
      return new ResponseEntity<>(roomService.markRoomsAsEmpty(roomIds), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }
}
