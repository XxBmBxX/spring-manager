package com.springmanger.controllers;

import com.springmanger.dtos.LoginCredentialsDto;
import com.springmanger.dtos.UserDto;
import com.springmanger.services.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

/**
 * The controller that takes care of the authentication.
 */
@RequiredArgsConstructor
@RestController
public class AuthenticationController {

  private final AuthenticationService authenticationService;

  @PostMapping("/log-in")
  public ResponseEntity<UserDto> loginUser(@RequestBody LoginCredentialsDto loginCredentialsDto) {
    return authenticationService.authenticateUser(loginCredentialsDto);
  }

  @PostMapping("/log-out")
  public ResponseEntity<Boolean> logoutUser(@RequestHeader("user-token") String userToken, @RequestBody UserDto userDto) {
    return authenticationService.logoutUser(userToken, userDto);
  }
}
