package com.springmanger.controllers;

import com.springmanger.exceptions.GuestDoesNotExistException;
import com.springmanger.models.groupedmodels.GuestGroup;
import com.springmanger.services.GuestGroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * The controller that will handle guest group requests.
 */
@RequiredArgsConstructor
@RestController
public class GuestGroupController {

  private final GuestGroupService guestGroupService;

  /**
   * Method that will save new guest group.
   *
   * @param guestGroup the body of the guest
   * @return the saved body
   */
  @PostMapping("/guest-group")
  public GuestGroup addGuestGroup(@RequestBody GuestGroup guestGroup) throws GuestDoesNotExistException {
    return guestGroupService.addGuestGroup(guestGroup);
  }

  /**
   * Method that returns all the guest groups in the database.
   *
   * @return all the guests
   */
  @GetMapping("/guest-groups")
  public Iterable<GuestGroup> getAllGuestGroups() {
    return guestGroupService.getAllGuestGroups();
  }
}
