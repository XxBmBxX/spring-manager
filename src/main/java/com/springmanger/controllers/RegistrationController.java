package com.springmanger.controllers;

import com.springmanger.models.UserCredentials;
import com.springmanger.services.RegistrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * The controller that has mappings for the registration requests.
 */
@RequiredArgsConstructor
@RestController
public class RegistrationController {

  private final RegistrationService registrationService;

  /**
   * The method that listens for post requests for registrations.
   *
   * @param userBody the json body that we receive
   * @return true or false
   */
  @PostMapping("/register")
  public ResponseEntity<Boolean> registerUser(@RequestBody UserCredentials userBody) {
    return registrationService.registerUser(userBody);
  }
}