package com.springmanger.controllers;

import com.springmanger.exceptions.GuestAlreadyExistsException;
import com.springmanger.models.Guest;
import com.springmanger.services.AuthenticationService;
import com.springmanger.services.GuestService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

/**
 * The controller that will handle guest requests.
 */
@RequiredArgsConstructor
@RestController
public class GuestController {

  private static final String USER_TOKEN_HEADER_NAME = "user-token";

  private final GuestService guestService;

  private final AuthenticationService authenticationService;

  /**
   * Method that will save new guest.
   *
   * @param guest the body of the guest
   * @return the saved body
   */
  @PostMapping("/guest")
  public ResponseEntity<Guest> addGuest(@RequestHeader(USER_TOKEN_HEADER_NAME) String userToken, @RequestBody Guest guest)
          throws GuestAlreadyExistsException {
    if (authenticationService.isAuthenticated(userToken)) {
      return new ResponseEntity<>(guestService.addGuest(guest), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  /**
   * Method that will edit guest.
   *
   * @param userToken authentication token
   * @param guest edited guest
   * @return true or false
   */
  @PatchMapping("/guest")
  public ResponseEntity<Boolean> editGuest(@RequestHeader(USER_TOKEN_HEADER_NAME) String userToken, @RequestBody Guest guest) {
    if (authenticationService.isAuthenticated(userToken)) {
      return guestService.editGuest(guest);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  /**
   * Method that returns all the guests in the database.
   *
   * @return all the guests
   */
  @GetMapping("/guests")
  public ResponseEntity<Iterable<Guest>> getAllGuests(@RequestHeader(USER_TOKEN_HEADER_NAME) String userToken) {
    if (authenticationService.isAuthenticated(userToken)) {
      return new ResponseEntity<>(guestService.getAll(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  /**
   * Method that deletes guests.
   *
   * @param userToken token we use to authenticate
   * @return true or false
   */
  @DeleteMapping("/guests")
  public ResponseEntity<Boolean> deleteGuests(@RequestHeader(USER_TOKEN_HEADER_NAME) String userToken, @RequestBody Iterable<Long> guestIds) {
    if (authenticationService.isAuthenticated(userToken)) {
      guestService.deleteAll(guestIds);
      return new ResponseEntity<>(true, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }
}
