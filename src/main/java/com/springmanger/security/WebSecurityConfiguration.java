package com.springmanger.security;

import java.util.Arrays;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * Security web config.
 */
@Configuration
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.cors().and().csrf().disable().authorizeRequests()
            .antMatchers("/").permitAll()
            .antMatchers(HttpMethod.POST, "/register").permitAll()
            .antMatchers(HttpMethod.POST, "/log-in").permitAll()
            .antMatchers(HttpMethod.POST, "/log-out").permitAll()
            .antMatchers(HttpMethod.POST, "/room").permitAll()
            .antMatchers(HttpMethod.GET, "/rooms").permitAll()
            .antMatchers(HttpMethod.POST, "/guest-group").permitAll()
            .antMatchers(HttpMethod.GET, "/guest-groups").permitAll()
            .antMatchers(HttpMethod.POST, "/guest").permitAll()
            .antMatchers(HttpMethod.POST, "/make-reservation").permitAll()
            .antMatchers(HttpMethod.PATCH, "/guest").permitAll()
            .antMatchers(HttpMethod.GET, "/guests").permitAll()
            .antMatchers(HttpMethod.DELETE, "/guests").permitAll()
            .antMatchers(HttpMethod.DELETE, "/delete-rooms").permitAll()
            .antMatchers(HttpMethod.PATCH, "/mark-rooms-as-clean").permitAll()
            .antMatchers(HttpMethod.PATCH, "/mark-rooms-as-empty").permitAll()
            .anyRequest().authenticated();
  }

  /**
   * Allowing cors methods.
   *
   * @return configuration
   */
  @Bean
  public CorsConfigurationSource corsConfigurationSource() {
    CorsConfiguration configuration = new CorsConfiguration();
    configuration.setAllowedOrigins(List.of("*"));
    configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
    configuration.setAllowedHeaders(Arrays.asList("authorization", "content-type", "x-auth-token", "user-token"));
    configuration.setExposedHeaders(List.of("x-auth-token"));
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", configuration);
    return source;
  }
}
