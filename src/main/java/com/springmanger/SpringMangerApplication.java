package com.springmanger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The starting point of the spring application.
 */
@SpringBootApplication
public class SpringMangerApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringMangerApplication.class, args);
  }
}
