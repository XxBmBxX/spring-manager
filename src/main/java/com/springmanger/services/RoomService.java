package com.springmanger.services;

import com.springmanger.dtos.MakeReservationDto;
import com.springmanger.exceptions.GuestDoesNotExistException;
import com.springmanger.models.Room;
import com.springmanger.models.enums.RoomStatus;
import com.springmanger.repositories.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * The service that will take care of the room logic.
 */
@RequiredArgsConstructor
@Service
public class RoomService {

  private final RoomRepository roomRepository;

  private final GuestGroupService guestGroupService;

  /**
   * Method that saves a room in the database.
   *
   * @param room the room we save
   * @return same room that was saved
   */
  public Room addRoom(Room room) {
    return roomRepository.save(room);
  }

  /**
   * Method that returns all the rooms in the database.
   *
   * @return all the rooms
   */
  public Iterable<Room> getAllRooms() {
    return roomRepository.findAll();
  }

  /**
   * Method that marks rooms as cleaned.
   *
   * @param roomIds the rooms to mark
   * @return the marked rooms
   */
  public Iterable<Room> markRoomsAsClean(Iterable<Long> roomIds) {
    Iterable<Room> cleanedRooms = roomRepository.findAllById(roomIds);
    cleanedRooms.forEach((room) -> room.setClean(true));

    return roomRepository.saveAll(cleanedRooms);
  }

  /**
   * Method that marks rooms as empty.
   *
   * @param roomIds the rooms to mark
   * @return the marked rooms
   */
  public Iterable<Room> markRoomsAsEmpty(Iterable<Long> roomIds) {
    Iterable<Room> emptiedRooms = roomRepository.findAllById(roomIds);
    emptiedRooms.forEach((room) -> {
      room.setGuestGroup(null);
      room.setRoomStatus(RoomStatus.AVAILABLE);
      room.setReservedFrom(null);
      room.setReservedTo(null);
      room.setReservationPrice(0);
    });

    return roomRepository.saveAll(emptiedRooms);
  }

  /**
   * Method that creates a reservation.
   *
   * @param makeReservationDto reservation dto
   * @return room
   */
  public Room makeReservation(MakeReservationDto makeReservationDto) throws GuestDoesNotExistException {
    Room room = roomRepository.getRoomByRoomNumber(makeReservationDto.getRoomNumber());
    room.setGuestGroup(guestGroupService.addGuestGroup(makeReservationDto.getGuestGroup()));
    room.setClean(false);
    room.setRoomStatus(RoomStatus.OCCUPIED);
    room.setReservedFrom(makeReservationDto.getReservedFrom());
    room.setReservedTo(makeReservationDto.getReservedTo());
    room.setReservationPrice(makeReservationDto.getReservationPrice());
    return roomRepository.save(room);
  }

  public void deleteRooms(Iterable<Long> roomIds) {
    roomRepository.deleteAllById(roomIds);
  }
}
