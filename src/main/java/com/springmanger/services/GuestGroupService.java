package com.springmanger.services;

import com.springmanger.exceptions.GuestDoesNotExistException;
import com.springmanger.models.Guest;
import com.springmanger.models.groupedmodels.GuestGroup;
import com.springmanger.repositories.GuestGroupRepository;
import com.springmanger.repositories.GuestRepository;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * The service that has the guest group logic.
 */
@RequiredArgsConstructor
@Service
public class GuestGroupService {

  private final GuestGroupRepository guestGroupRepository;

  private final GuestRepository guestRepository;

  public GuestGroup addGuestGroup(GuestGroup guestGroup) throws GuestDoesNotExistException {
    guestGroup.setGuests(fillGuestGroup(guestGroup));
    return guestGroupRepository.save(guestGroup);
  }

  public Iterable<GuestGroup> getAllGuestGroups() {
    return guestGroupRepository.findAll();
  }

  private Set<Guest> fillGuestGroup(GuestGroup guestGroup) throws GuestDoesNotExistException {
    try {
      return guestGroup.getGuests()
              .stream()
              .map(guest -> guestRepository.findById(guest.getId()).orElseThrow())
              .collect(Collectors.toSet());
    } catch (NoSuchElementException elementException) {
      throw (GuestDoesNotExistException) new GuestDoesNotExistException("Such guest does not exist.").initCause(elementException);
    }
  }
}
