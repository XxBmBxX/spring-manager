package com.springmanger.services;

import com.springmanger.models.UserCredentials;
import com.springmanger.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * The class that takes care of the registration of new user.
 */
@RequiredArgsConstructor
@Service
public class RegistrationService {

  private final UserRepository userRepository;

  private final BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();

  /**
   * The method that saves the new user in the database.
   *
   * @param userRaw the raw user we receive from the post request
   * @return true or false
   */
  public ResponseEntity<Boolean> registerUser(UserCredentials userRaw) {
    boolean successfulRegistration = false;
    if (!checkForUsername(userRaw.getUsername())) {
      userRepository.save(
              new UserCredentials(
                      userRaw.getFirstName(),
                      userRaw.getLastName(),
                      userRaw.getUsername(),
                      bcryptPasswordEncoder.encode(userRaw.getPassword()))
      );
      successfulRegistration = true;
    }

    return new ResponseEntity<>(successfulRegistration, HttpStatus.OK);
  }

  /**
   * Checks if user with this name exists in the database.
   *
   * @param username the username we have to check
   * @return boolean
   */
  private boolean checkForUsername(String username) {
    return userRepository.existsByUsername(username);
  }
}
