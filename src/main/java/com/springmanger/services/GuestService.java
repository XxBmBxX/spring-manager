package com.springmanger.services;

import com.springmanger.exceptions.GuestAlreadyExistsException;
import com.springmanger.models.Guest;
import com.springmanger.models.Room;
import com.springmanger.models.groupedmodels.GuestGroup;
import com.springmanger.repositories.GuestGroupRepository;
import com.springmanger.repositories.GuestRepository;
import com.springmanger.repositories.RoomRepository;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * The service for guests.
 */
@RequiredArgsConstructor
@Service
public class GuestService {

  private final GuestRepository guestRepository;

  private final GuestGroupRepository guestGroupRepository;

  private final RoomRepository roomRepository;

  private final RoomService roomService;

  /**
   * The method that saves new guest.
   *
   * @param guest the guest we save
   * @return the saved guest
   * @throws GuestAlreadyExistsException thrown when guest does not exist.
   */
  public Guest addGuest(Guest guest) throws GuestAlreadyExistsException {
    if (guestRepository.findGuestByFirstNameAndLastName(guest.getFirstName(), guest.getLastName()).isEmpty()) {
      return guestRepository.save(guest);
    } else {
      throw new GuestAlreadyExistsException("Already exists");
    }
  }

  /**
   * Method that saves the edited guest.
   *
   * @param guest edited guest
   * @return new response entity
   */
  public ResponseEntity<Boolean> editGuest(Guest guest) {
    guestRepository.save(guest);

    return new ResponseEntity<>(true, HttpStatus.OK);
  }

  /**
   * Method that returns all guests in the database.
   *
   * @return guests
   */
  public Iterable<Guest> getAll() {
    return guestRepository.findAll();
  }

  /**
   * Method to safely delete guests.
   *
   * @param guestIds the ids of the users to delete
   */
  public void deleteAll(Iterable<Long> guestIds) {
    Set<Guest> foundGuests = StreamSupport.stream(guestRepository.findAllById(guestIds).spliterator(), false).collect(Collectors.toSet());
    Iterable<GuestGroup> foundGuestGroups = guestGroupRepository.findAllByGuestsIn(foundGuests);
    foundGuestGroups.forEach((guestGroup) -> foundGuests.forEach((guest) -> guestGroup.getGuests().remove(guest)));

    guestGroupRepository.saveAll(foundGuestGroups);
    guestRepository.deleteAllById(guestIds);
    roomService.markRoomsAsEmpty(StreamSupport.stream(roomRepository.findAll().spliterator(), false)
        .filter((room) -> room.getGuestGroup() != null && room.getGuestGroup().getGuests().size() == 0).map(Room::getId)
        .collect(Collectors.toList()));
  }
}
