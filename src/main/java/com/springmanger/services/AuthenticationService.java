package com.springmanger.services;

import com.springmanger.dtos.LoginCredentialsDto;
import com.springmanger.dtos.UserDto;
import com.springmanger.models.UserAuthentication;
import com.springmanger.models.UserCredentials;
import com.springmanger.repositories.UserAuthenticationRepository;
import com.springmanger.repositories.UserRepository;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * The service that will manage authenticated users.
 */
@RequiredArgsConstructor
@Service
public class AuthenticationService {

  private final UserRepository userRepository;

  private final UserAuthenticationRepository userAuthenticationRepository;

  private final BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
  private final HashMap<String, String> userTokens = new HashMap<>();

  /**
   * The method that will check if the credentials are right.
   *
   * @param loginCredentialsDto the info we use to authenticate
   * @return the information of the user
   */
  public ResponseEntity<UserDto> authenticateUser(LoginCredentialsDto loginCredentialsDto) {
    Optional<UserCredentials> userCredentials = userRepository.findUserCredentialsByUsername(loginCredentialsDto.getUsername());

    if (userCredentials.isPresent() && bcryptPasswordEncoder.matches(loginCredentialsDto.getPassword(), userCredentials.get().getPassword())) {
      HttpHeaders responseHeaders = new HttpHeaders();
      String userToken = UUID.randomUUID().toString();
      userTokens.put(userCredentials.get().getUsername(), userToken);

      userAuthenticationRepository.save(new UserAuthentication(userCredentials.get().getUsername(), userToken));

      responseHeaders.add("user-token", userTokens.get(userCredentials.get().getUsername()));
      responseHeaders.set("Access-Control-Expose-Headers", "user-token");

      return new ResponseEntity<>(new UserDto(userCredentials.get()), responseHeaders, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  /**
   * Method that checks if the user token is valid.
   *
   * @param userToken UUID
   * @return boolean
   */
  public boolean isAuthenticated(String userToken) {
    return userTokens.containsValue(userToken);
  }

  /**
   * Method that logs the user out.
   *
   * @param userToken the token of the user
   * @param userDto the user object
   * @return response entity
   */
  public ResponseEntity<Boolean> logoutUser(String userToken, UserDto userDto) {
    if (userTokens.remove(userDto.getUsername(), userToken)) {
      Iterable<UserAuthentication> userAuthentication = userAuthenticationRepository.findAllByUsername(userDto.getUsername());
      userAuthenticationRepository.deleteAll(userAuthentication);

      return new ResponseEntity<>(true, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
    }
  }

  /**
   * Method that will run everytime the app is started to get the saved user tokens.
   */
  @PostConstruct
  public void getPersistedTokens() {
    userAuthenticationRepository.findAll().forEach(
            (userAuthentication) -> userTokens.put(userAuthentication.getUsername(), userAuthentication.getAuthenticationToken())
    );
  }
}
