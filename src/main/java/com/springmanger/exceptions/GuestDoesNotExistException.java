package com.springmanger.exceptions;

import java.io.Serial;

/**
 * Exception for when player does not exist.
 */
public class GuestDoesNotExistException extends Exception {
  @Serial
  private static final long serialVersionUID = 42L;

  public GuestDoesNotExistException(Throwable cause) {
    super(cause);
  }

  public GuestDoesNotExistException(String message) {
    super(message);
  }
}
