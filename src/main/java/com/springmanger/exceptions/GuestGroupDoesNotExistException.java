package com.springmanger.exceptions;

import java.io.Serial;

/**
 * Exception for when guest group does not exist.
 */
public class GuestGroupDoesNotExistException extends Exception {
  @Serial
  private static final long serialVersionUID = 42L;

  public GuestGroupDoesNotExistException(Throwable cause) {
    super(cause);
  }

  public GuestGroupDoesNotExistException(String message) {
    super(message);
  }
}
