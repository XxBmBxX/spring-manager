package com.springmanger.exceptions;

import java.io.Serial;

/**
 * Exception for when guest already exists.
 */
public class GuestAlreadyExistsException extends Exception {
  @Serial
  private static final long serialVersionUID = 42L;

  public GuestAlreadyExistsException(Throwable cause) {
    super(cause);
  }

  public GuestAlreadyExistsException(String message) {
    super(message);
  }
}
