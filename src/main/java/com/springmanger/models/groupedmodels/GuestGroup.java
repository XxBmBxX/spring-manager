package com.springmanger.models.groupedmodels;

import com.springmanger.models.Guest;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import lombok.Getter;
import lombok.Setter;

/**
 * The entity that will hold information about
 * notes for a reservation guest group.
 */
@Getter
@Setter
@Entity(name = "guest_group")
public class GuestGroup {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "guest_group_id", nullable = false)
  private Long id;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(
          name = "guest_group_join",
          joinColumns = @JoinColumn(name = "guest_group_id"),
          inverseJoinColumns = @JoinColumn(name = "guest_id")
  )

  private Set<Guest> guests;

  @OneToOne(cascade = CascadeType.ALL)
  private NoteGroup noteGroup;
}
