package com.springmanger.models.groupedmodels;

import com.springmanger.models.Note;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import lombok.Getter;
import lombok.Setter;

/**
 * The entity that will hold information about
 * notes for a reservation guest group.
 */
@Getter
@Setter
@Entity(name = "note_group")
public class NoteGroup {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "note_group_id", nullable = false)
  private Long id;

  @ManyToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
  @JoinTable(
          name = "note_group_join",
          joinColumns = @JoinColumn(name = "note_group_id"),
          inverseJoinColumns = @JoinColumn(name = "note_id")
  )
  @OrderColumn(name = "index")
  private Set<Note> notes;
}
