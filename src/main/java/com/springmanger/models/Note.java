package com.springmanger.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

/**
 * The entity that will hold information about
 * notes for a reservation guest group.
 */
@Getter
@Setter
@Entity(name = "note")
public class Note {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "note_id", nullable = false)
  private Long id;

  @Column(name = "note")
  private String noteText;
}
