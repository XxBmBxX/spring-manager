package com.springmanger.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

/**
 * The entity that will hold the user information.
 */
@Getter
@Setter
@Entity(name = "user_credentials")
public class UserCredentials {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(nullable = false)
  private Long id;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "username")
  private String username;

  @Column(name = "password")
  private String password;

  public UserCredentials() { }

  /**
   * Constructor with args.
   *
   * @param firstName the user first name
   * @param lastName the user last name
   * @param username the user username
   * @param password the user encrypted password
   */
  public UserCredentials(String firstName,
              String lastName,
              String username,
              String password) {

    this.firstName = firstName;
    this.lastName = lastName;
    this.username = username;
    this.password = password;
  }
}
