package com.springmanger.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.springmanger.models.enums.RoomStatus;
import com.springmanger.models.enums.RoomType;
import com.springmanger.models.groupedmodels.GuestGroup;
import java.sql.Timestamp;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import lombok.Getter;
import lombok.Setter;

/**
 * Entity that will hold information about the room's state.
 */
@Getter
@Setter
@Entity(name = "room")
public class Room {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "room_id", nullable = false)
  private Long id;

  @Column(name = "room_number")
  private Integer roomNumber;

  @Column(name = "room_type")
  private RoomType roomType;

  @Column(name = "is_clean")
  @JsonProperty("isClean")
  private boolean isClean;

  @Column(name = "reserved_from")
  private Timestamp reservedFrom;

  @Column(name = "reserved_to")
  private Timestamp reservedTo;

  @Column(name = "room_status")
  private RoomStatus roomStatus;

  @Column(name = "reservation_price")
  private Integer reservationPrice;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "guest_group_fk")
  private GuestGroup guestGroup;
}
