package com.springmanger.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

/**
 * The entity that will hold information
 * for single guest.
 */
@Getter
@Setter
@Entity(name = "user_authentication")
public class UserAuthentication {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", nullable = false)
  private Long id;

  @Column(name = "username")
  private String username;

  @Column(name = "authentication_token")
  private String authenticationToken;

  public UserAuthentication() {
  }

  public UserAuthentication(String username, String authenticationToken) {
    this.username = username;
    this.authenticationToken = authenticationToken;
  }
}