package com.springmanger.models.enums;

/**
 * Enums that describe the room type.
 */
public enum RoomType {
  SINGLE,
  DOUBLE,
  FAMILY
}
