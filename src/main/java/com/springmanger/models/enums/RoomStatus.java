package com.springmanger.models.enums;

/**
 * Enums that describe the room status.
 */
public enum RoomStatus {
  AVAILABLE,
  OCCUPIED,
  RESERVED,
}
